var allowedIPs = ["111.111.111.111", "222.222.222.222"];
// Allowed modes: normal, maintenance, queue
var mode = "queue"
var outflowRate = 50
var cookieExpiration = 1800

addEventListener("fetch", event => {
  event.respondWith(fetchAndReplace(event.request))
})

function byteStringToUint8Array(byteString) {
  const ui = new Uint8Array(byteString.length)
  for (let i = 0; i < byteString.length; ++i) {
    ui[i] = byteString.charCodeAt(i)
  }
  return ui
}

function getCookie(cookies, cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(cookies);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

async function fetchAndReplace(request) {

  var date = new Date();
  var timestamp = date.getTime();
  let modifiedHeaders = new Headers()
  var queue_count = await QUEUE_NS.get("queue_count")

  modifiedHeaders.set('Content-Type', 'text/html')
  modifiedHeaders.append('Pragma', 'no-cache')

  const encoder = new TextEncoder()
  const secretKeyData = encoder.encode("my secret symmetric key")
  const key = await crypto.subtle.importKey(
    "raw", secretKeyData,
    { name: "HMAC", hash: "SHA-256" },
    false, [ "sign", "verify" ]
  )

  //Return maint page if you're not calling from a trusted IP
  if (allowedIPs.indexOf(request.headers.get("cf-connecting-ip")) < 0 && mode != "normal") 
  {
		//var queue_id = await QUEUE_NS.get("queue_id")
		var queue_position = await QUEUE_NS.get("queue_position")
    var queue_fields = queue_position.split("-")
		if (Number(queue_fields[0]) + 60000 < timestamp) {
      await QUEUE_NS.put("queue_position", timestamp + "-" + (Number(queue_fields[1]) + outflowRate)) 
		}

    let cookies = request.headers.get('Cookie') || ""
		if (cookies.includes("queue")) {
			cookieData = getCookie(cookies, "queue").split("-")
			queue_id = cookieData[0]
			const receivedMac = byteStringToUint8Array(atob(cookieData[1]))
			const verified = await crypto.subtle.verify("HMAC", key, receivedMac, encoder.encode(queue_id))
			if((Number(queue_id) < Number(queue_fields[1]) + outflowRate) && mode === 'queue' && verified) {
				return fetch(request)
			}
		} else {
		  queue_id = Number(queue_fields[1]) + 1
		  await QUEUE_NS.put("queue_count", Number(queue_count) + 1)
		  const signature = await crypto.subtle.sign("HMAC", key, encoder.encode(queue_id));
		  const base64Mac = btoa(String.fromCharCode(...new Uint8Array(signature)))
			modifiedHeaders.append("Set-Cookie", "queue=" + queue_id + "-"  + base64Mac + "; Max-Age=1800; Path=/")
		}

    page = pageTemplate.replace("{{ queue_data }}", queue_id + " qp: " + queue_position)
    // Return modified response.
    return new Response(page, {
      status: 503,
      headers: modifiedHeaders
    })
  }
  else //Allow users from trusted into site
  {
    //Fire all other requests directly to our WebServers
    await QUEUE_NS.put("queue_count", Number(queue_count) - 1)
    return fetch(request)
  }
}

let pageTemplate = `
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="refresh" content="10"/>
</head>
<body>
<p>{{ queue_data }}</p>
</body>
</html>
`;

let maintenanceTemplate = `
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="refresh" content="10"/>
</head>
<body>
<p>Under maintenance</p>
</body>
</html>
`;
