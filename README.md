# Queue access system for your site implemented as CloudFlare worker

Work in progress, feedback appreciated

## What does it do

Provides an interstitial access page hosted at CloudFlare edge, puts your visitors in the queue and releases them onto your site at a controlled rate (users per minute) and/or at the specified date/time.

## What to use it for

Limited stock sale events, high traffic events

## How it works

Uses CloudFlare workers and recently relased (Beta) CloudFlare distributed Key-Value store

## Prerequisites

Workers and KV store enabled on your CloudFlare account

## Setup

1. Create Workers KV namespace, for example "QUEUE"
1. In Workers editor->Resources, create Namespace binding, for example NS_QUEUE = QUEUE 
1. Use NS_QUEUE.put and NS_QUEUE.get in the worker script

## Parameters

* outflowRate - how many users are released from queue to your site per minute
* eventStart - UTC timestamp when user release from the queue will start

## Contact

Please contact me at negrusti@gmail.com for implementation details

## Credits

Original maintenance page idea and code is borrowed from  
https://www.resdevops.com/2018/03/20/cloudflare-workers-maintenance-mode-static-page/

## Todo

* Cookie signing/encryption to prevent tampering
* Single page GUI (PHP?) to modify KV values, enable and disable the queue
* Queue page template design
* VIP users
* Google Analytics - split monitoring for queue page and actual page

## Ideas

* Load monitoring system can dynamically adjust queue outflow rate depending on desired load parameters
